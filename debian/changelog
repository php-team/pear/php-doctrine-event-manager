php-doctrine-event-manager (2.0.1-2) unstable; urgency=medium

  * Use --require-file for phpabtl (Closes: #1074195)

 -- David Prévot <taffit@debian.org>  Sun, 14 Jul 2024 19:44:43 +0200

php-doctrine-event-manager (2.0.1-1) unstable; urgency=medium

  [ Alexander M. Turek ]
  * PHPUnit 10.5 (#70)

  [ David Prévot ]
  * Update Standards-Version to 4.7.0

 -- David Prévot <taffit@debian.org>  Fri, 24 May 2024 07:54:10 +0200

php-doctrine-event-manager (2.0.0-2) unstable; urgency=medium

  * Upload to unstable since everything is ready
  * Update standards version to 4.6.2, no changes needed.

 -- David Prévot <taffit@debian.org>  Sun, 11 Jun 2023 16:38:14 +0200

php-doctrine-event-manager (2.0.0-1) experimental; urgency=medium

  * Upload new major to experimental

  [ Alexander M. Turek ]
  * Bump to PHP 8.1 and add types everywhere (#51)
  * Revert return type on EventSubscriber::getSubscribedEvents() (#54)
  * Made the $event parameter of EventManager::getListeners() mandatory (#57)
  * Remove doctrine/deprecations (#59)

  [ David Prévot ]
  * Revert "Track version 1 for now (Bookworm?)"
  * Drop php-doctrine-deprecations build-dependency

 -- David Prévot <taffit@debian.org>  Sat, 15 Oct 2022 12:15:37 +0200

php-doctrine-event-manager (1.2.0-1) unstable; urgency=medium

  [ Alexander M. Turek ]
  * Flatten directory structure (#49)

  [ Maciej Malarz ]
  * Failsafe for unknown event (#61)

  [ David Prévot ]
  * Track version 1 for now (Bookworm?)
  * Adapt packaging to new layout

 -- David Prévot <taffit@debian.org>  Sat, 15 Oct 2022 11:47:58 +0200

php-doctrine-event-manager (1.1.2-1) unstable; urgency=medium

  [ Nic Wortel ]
  * Require PHPUnit 7, 8 or 9

  [ David Prévot ]
  * Simplify phpunit call

 -- David Prévot <taffit@debian.org>  Sat, 30 Jul 2022 11:05:31 +0200

php-doctrine-event-manager (1.1.1-2) unstable; urgency=medium

  * Simplify gbp import-orig (and check signature)
  * Fix field name case in debian/tests/control (Test-command => Test-Command).
  * Mark package as Multi-Arch: foreign
  * Use dh-sequence-phpcomposer instead of --with phpcomposer
  * Install and use phpabtpl(1) autoloaders
  * Update Standards-Version to 4.6.1

 -- David Prévot <taffit@debian.org>  Mon, 27 Jun 2022 07:09:53 +0200

php-doctrine-event-manager (1.1.1-1) unstable; urgency=medium

  [ Grégoire Paris ]
  * Allow PHP 8

  [ David Prévot ]
  * Set upstream metadata fields:
    Bug-Database, Bug-Submit, Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Set Rules-Requires-Root: no.
  * Use debhelper-compat 13
  * Simplify override_dh_auto_test

 -- David Prévot <taffit@debian.org>  Sat, 08 Aug 2020 07:47:46 +0200

php-doctrine-event-manager (1.1.0-1) unstable; urgency=medium

  [ Jonathan H. Wage ]
  * Add .doctrine-project.json to root of the project.
  * Add .github/FUNDING.yml

  [ Michael Moravec ]
  * Apply Doctrine CS 5.0
  * CI: Test against PHP 7.3
  * CI: Test against PHP 7.4snapshot instead of nightly (8.0)

  [ Grégoire Paris ]
  * Link to Slack instead of Gitter

  [ Patrick Jahns ]
  * Updated doctrine/coding-standard to 6.0

  [ Sam Reed ]
  * Update .gitattributes

  [ David Prévot ]
  * Update gbp import-ref usage
  * d/control:
    + Drop versioned dependency satisfied in (old)stable
    + Update Description
    + Update standards version to 4.4.1, no changes needed.
  * d/u/metadata: Set upstream metadata fields: Repository.

 -- David Prévot <taffit@debian.org>  Mon, 11 Nov 2019 18:38:14 -1000

php-doctrine-event-manager (1.0.0+git-1) unstable; urgency=medium

  * Use upstream testsuite
  * Bump debhelper from old 11 to 12.
  * Update Standards-Version to 4.4.0
  * Document gbp import-ref usage

 -- David Prévot <taffit@debian.org>  Sat, 24 Aug 2019 11:34:19 -1000

php-doctrine-event-manager (1.0.0-1) unstable; urgency=medium

  * Initial release
  * New php-doctrine-dbal dependency

 -- David Prévot <taffit@debian.org>  Tue, 17 Jul 2018 10:06:48 +0800
